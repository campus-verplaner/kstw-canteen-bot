# KStW Canteen Bot

A bot which posts the canteen menu into a discord channel via a webhook, along with amusing messages.

## Installation

Install all dependencies (`shards`, `crystal`, `libxml2`), then run:
```sh
shards build
```

The executable should now be inside the `bin/` directory.

## Usage

```
$ kstw_canteen_bot --help
Usage: kstw_canteen_bot [arguments]
    --webhook PATH                   Sets the webhook path
    --date DATE                      Sets the requested date [format: YYYY-MM-DD] (default: NOW)
    --location NAME                  Sets the requested location [format: ID or NAME] (default: 22)
    -h, --help                       Show this help
```

You need to pass the relevant information directly via the CLI.
Example:
```bash
kstw_canteen_bot --webhook "https://discord.com//api/webhooks/REDACTED/REDACTED" --date "2021-08-20" --location "Standort Zülpicher Straße"
```

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/campus-verplaner/kstw-canteen-bot/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [David Keller](https://gitlab.com/BlobCodes) - creator and maintainer
