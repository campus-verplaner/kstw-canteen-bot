record Dish,
  name : String,
  description : String,
  image_url : String,
  prices : String,
  additives : Array(String),
  time_begin : Time?,
  time_end : Time?,
  location_part : String?,
  menu_line : String
