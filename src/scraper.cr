require "xml"
require "http/client"
require "./models"

module Scraper
  def self.fetch_dishes(date : Time, location : String) : Array(Dish)
    date = date.at_beginning_of_day
    res = HTTP::Client.get("https://sw-koeln.maxmanager.xyz/index.php")
    raise RuntimeError.new("HTTP Status #{res.status_code}") unless res.success?

    document = XML.parse_html(res.body)
    dishes_html = document.xpath_nodes("//div[@data-einrichtung=\"#{location}\"]//div[@class=\"col-12 col-lg-6 mb-4 hide essensblock\"][@data-essensdatum=\"#{date.to_s("%F")}\"]")

    dishes_html.map do |dish|
      additives_node = dish.xpath_node(".//div[@class=\"info-legende\"]/i")
      raise RuntimeError.new("Missing additives") unless additives_node

      Dish.new(
        name: dish.xpath_string("normalize-space(.//div[@class=\"essenstext ubuntu-bold\"]/text())"),
        prices: dish.xpath_string("normalize-space(.//div[@class=\"preise\"]/text())"),
        description: dish.xpath_string("normalize-space(.//div[@class=\"beschreibungtext\"]/text())"),
        menu_line: dish["data-menuelinie"],
        additives: additives_node["data-allerg"].split("<br>", remove_empty: true) + additives_node["data-zusatz"].split("<br>", remove_empty: true) + additives_node["data-sonst"].split("<br>", remove_empty: true),
        location_part: nil,
        image_url: "https://sw-koeln.maxmanager.xyz/" + dish.xpath_string("normalize-space(.//img[2]/@src)"),
        time_begin: nil,
        time_end: nil,
      )
    end
  end
end
