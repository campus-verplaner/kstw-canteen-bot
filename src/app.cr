require "option_parser"
require "json"
require "http/client"
require "./models"
require "./scraper"

class App
  USERNAME  = "Mensa-Geschmacksorakel"
  AVATAR    = "https://cdn.discordapp.com/attachments/1177337996991795250/1177347586538094652/320973087292211.png"

  ADDITIVES_TEMPLATE = ->(additives : Array(String)) { additives.empty? ? "(Keine Daten)" : "Im Gericht: #{additives.join(", ")}" }
  TIME_TEMPLATE      = ->(b : Time, e : Time) { "#{b.to_s("%R")}–#{e.to_s("%R")}" }

  PRICE_STRING    = "Preis"
  LOCATION_STRING = "Ort"
  TIME_STRING     = "Zeit"

  MENU_LINES = {
    "WORLDWIDE"   => {color: 0x0182A8, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344994147835984/worldwide.png"},
    "QUERBEET"    => {color: 0x429E12, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344993669693530/querbeet.png"},
    "HEIMSPIEL"   => {color: 0xC92423, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344993925533766/heimspiel.png"},
    "MEISTERWERK" => {color: 0x7D7869, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344994386919504/meisterwerk.png"},
    "STREETFOOD"  => {color: 0xE36B0D, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344994600816690/streetfood.png"},
    "FALLBACK"    => {color: 0xFFC71F, icon: "https://cdn.discordapp.com/attachments/1177337996991795250/1177344993438998568/fallback.png"},
  }

  MESSAGES = begin
    {% begin %}
    {%
      messages = <<-HTML
    Guten Morgen, meine kulinarischen Freunde! 🌞🍽️

    Die Sonne strahlt, die Vögel zwitschern, und in der Küche wurde fleißig gewerkelt, um euch heute ein besonderes Leckerbissen zu präsentieren. 🐦👨‍🍳

    🍽️ Hier ist das köstliche Menü für den Tag in unserer geliebten Mensa: 🍽️

    Ich hoffe, ihr genießt eure Mahlzeit genauso sehr wie Oma's hausgemachte Küchlein! 😊🍰 Guten Appetit! 🍽️😋
    ---
    Hallo meine lieben Discord-Gourmets! 🍽️💕

    Heute hat die Küche mal wieder den Kochlöffel geschwungen, um euch mit einer köstlichen Menüauswahl zu verwöhnen. 🍲 Oma würde stolz sein! 👵

    🍽️ Hier ist das heutige Menü in der Mensa: 🍽️

    Guten Appetit und lasst es euch schmecken! 🍽️😋
    ---
    Heyho, Feinschmecker! 🍽️🎉

    Die Mensa hat heute etwas ganz Besonderes für euch vorbereitet. 🍲🥂

    👩‍🍳 Hier das heutige Menü in der Mensa: 👩‍🍳

    Wehe es schmeckt euch nicht! 🍽️😋
    ---
    Moin moin, ihr Schlemmermäuler! 🌅🍛

    Es ist wieder Zeit, eure Geschmacksknospen auf eine Reise zu schicken. 🍽️🌍

    🍽️ Hier präsentiere ich euch stolz das köstliche Menü des Tages in unserer geliebten Mensa: 🍽️

    Lasst euch von den Aromen verzaubern und genießt jeden Bissen, als wäre es ein Stück aus Großmutters Geheimrezeptbuch! 😋👵 Guten Appetit! 🍽️🍂
    ---
    Heyho, ihr Naschkatzen! 🍽️🍰

    Die ersten Sonnenstrahlen des Tages kitzeln eure Nasen, während das Küchenteam bereits fleißig am Werk ist, um euch heute ein unvergessliches Geschmackserlebnis zu bereiten. 👨‍🍳🍳

    🍽️ Hier ist das heutige Menü, frisch aus der Mensa: 🍽️

    Schnappt euch eine Gabel, denn heute wird geschlemmt, als gäbe es kein Morgen! 😋🎉
    ---
    Ahoi, ihr Landratten und Matrosen der Gaumenfreuden! ☠️🍽️

    Der Koch hat seine Schlachtplanung abgeschlossen, und hier ist das Menü für den heutigen Beutezug in der Mensa: 🏴‍☠️🍽️

    Setzt eure Piratenhüte auf und schnappt euch eure Gabeln, denn heute plündern wir die Schätze des Geschmacks! 😄💰
    ---
    Willkommen zur kulinarischen Schlacht, meine Gourmet-Gladiatoren! 🍳🔥

    Die Küche ist heute die Arena, und es wurde ein Menü zusammengestellt, das so sensationell ist, als wären wir in einer Shokugeki! 👨‍🍳🍽️

    🍽️ Hier ist das geschmackvolle Duell des Tages aus der Mensa: 🍽️

    Nehmt eure Kochutensilien in die Hand und zeigt, dass ihr wahre Meister des Geschmacks seid! 💪😋
    ---
    Heyho, meine kochbegeisterten Abenteurer und Gewürz-Gourmets! 🌶️🌟

    Heute wird euch ein Menü serviert, das so umwerfend ist, als würde der Erste Sitz selbst in unserer Küche stehen! 👨‍🍳🌟

    🍽️ Hier ist das kulinarische Manifest des Tages aus der Mensa: 🍽️

    Macht euch bereit für eine scharfe Schlacht, die eure Geschmacksknospen fordern wird! 🔥🌮
    ---
    Ahoi, hungrige Matrosen! 😋

    Wir eröffnen eine Schlacht gegen die Krosse Krabbe, selbst ohne die Geheimformel! 🌴🍛

    🍽️ Hier ist das überwältigende Menü des Tages aus der Mensa: 🍽️

    Nach diesem Erlebnis wird nur noch die Mensa sich eurem Magen als würdig erweisen! 🔥🌮
    ---
    Guten Morgen, meine Daten-Detektive und Gourmet-Entwickler! 🌞🖥️

    Die Sonne ist aufgegangen, die Vögel zwitschern, und in der Küche wurde fleißig programmiert, um euch heute ein einzigartiges Menü zu präsentieren! 💾

    🍽️ Hier ist das datengetriebene Menü des Tages: 🍽️

    Ich hoffe, euer Geschmackssensor ist bereit für ein geschmackvolles Datenbank-Update! 🍲🤓
    ---
    Guten Tag, meine hungrigen Helden des Campus! 🌟🍽️

    Heute erwartet euch ein kulinarisches Abenteuer, das eure Geschmacksknospen in pure Ekstase versetzen wird. Lasst uns gemeinsam in die Welt der Gaumenfreuden eintauchen! 🌊👨‍🍳

    🍽️ Hier ist das magische Menü des Tages in unserer Mensa: 🍽️

    Lasst es euch schmecken und erinnert euch: Ein gutes Essen ist das beste Mittel gegen jeden Prüfungstress! 😊📚🍲 Guten Appetit! 😋
    ---
    Seid gegrüßt, meine geschmackvollen Entdecker! 🌍🍴

    Heute haben unsere Küchenzauberer ein Menü kreiert, das so verlockend ist, dass selbst ein Drache seinen Schatz dafür hergeben würde. 🐉👨‍🍳

    🍽️ Hier ist der kulinarische Schatz des Tages aus der Mensa: 🍽️

    Vergesst nicht: Wer gut isst, der studiert auch besser! 🍀📚 Bon Appétit! 😋🍽️
    ---
    Hallo, meine Meister der Mittagspause! 🍲🏆

    Heute wird die Mensa zur Bühne, und ihr seid die VIP-Gäste eines Geschmackserlebnisses, das in die Geschichtsbücher eingehen könnte. 📜👨‍🍳

    🍽️ Hier ist das glanzvolle Menü des Tages: 🍽️

    Lasst euch verzaubern und vergesst nicht: Ein volles Magen zaubert die besten Ideen hervor! 💡😋 Guten Appetit! 🍴✨
    ---
    Willkommen, meine hungrigen Entdecker des Geschmacks! 🚀🍽️

    Heute nehmen wir euch mit auf eine kulinarische Reise, die euch zu den Sternen und wieder zurück führt. 🌟👨‍🍳

    🍽️ Hier ist das galaktische Menü des Tages in unserer Mensa: 🍽️

    Greift zu und genießt: Der Weg zu den Sternen beginnt mit einem guten Essen! 🚀🍲 Guten Appetit! 😋
    ---
    Willkommen, meine Galaxienreisenden und Sternenforscher! 🌌🍽️

    Heute bringt euch die Mensa ein Menü, das so außergewöhnlich ist, dass selbst die Crew der Enterprise an Bord kommen würde. Macht euch bereit für einen Geschmacks-Hyperraum-Sprung! 🚀👨‍🍳

    🍽️ Hier ist das interstellare Menü des Tages: 🍽️

    Ladet eure Gabeln auf und genießt: Ein voller Magen ist die beste Vorbereitung für jede Mission! 🍴😋 Guten Appetit! 🌠✨
    ---
    Grüße, Gaming-Gurus und Konsolen-Helden! 🎮🍽️

    Die Mensa hat heute ein Menü vorbereitet, das eure Energieleisten auffüllt und euch bereit für die nächste Quest macht. 🍽️

    🍽️ Hier ist das heutige Menü: 🍽️

    Genießt euer Essen und möge der Highscore mit euch sein! 😋🎮
    ---
    Konnichiwa, Anime-Fans und Manga-Leser! 🍱🍽️

    Heute gibt es in der Mensa ein Menü, das selbst in eurem Lieblingsanime eine Hauptrolle spielen könnte. 🍙

    🍽️ Hier ist das heutige Menü: 🍽️

    Lasst es euch schmecken und genießt jede Episode eurer Mahlzeit! 😋🍱
    ---
    Hey, Musikliebhaber und Bandmitglieder! 🎸🍽️

    Heute gibt es in der Mensa ein Menü, das so harmonisch ist wie eure Lieblingsmelodie. Lasst es euch schmecken! 🎼

    🍽️ Hier ist das musikalische Menü des Tages: 🍽️

    Guten Appetit und bleibt im Takt! 😋🎵
    ---
    Guten Morgen, liebe Campus-Abenteurer! 🏫🍽️

    Heute hat unsere Mensa ein Menü vorbereitet, das euch perfekt durch den Uni-Tag bringen wird. Macht euch bereit für eine köstliche Stärkung! 👨‍🍳

    🍽️ Hier ist das heutige Campus-Menü: 🍽️

    Guten Appetit und viel Erfolg bei euren Vorlesungen! 😋📚
    ---
    Hallo, Bibliotheksbewohner und Lernhelden! 📖🍽️

    Die Mensa hat heute ein Menü kreiert, das euch die nötige Energie für lange Lerntage gibt. Lasst es euch schmecken! 🍲

    🍽️ Hier ist das heutige Menü für unsere fleißigen Studierenden: 🍽️

    Genießt eure Mahlzeit und happy studying! 😋📖
    ---
    Grüße, kreative Köpfe und Kunstliebhaber! 🎨🍽️

    Heute gibt es in der Mensa ein Menü, das so bunt und kreativ ist wie eure besten Werke. Lasst euch inspirieren! 🖌️

    🍽️ Hier ist das künstlerische Menü des Tages: 🍽️

    Genießt eure Mahlzeit und bleibt kreativ! 😋🎨
    ---
    Hallo, Forschungstalente und Wissenschaftler! 🔬🍽️

    Unsere Mensa hat heute ein Menü zusammengestellt, das euch die nötige Energie für eure Forschungsprojekte liefert. 🧪

    🍽️ Hier ist das wissenschaftliche Menü des Tages: 🍽️

    Guten Appetit und viel Erfolg im Labor! 😋📚
    ---
    Dem Sprücheschreiber sind die Ideen ausgegangen.. 😢

    Bitte helfen: https://fachschaftgm.de/nextcloud/apps/forms/s/S7TPXS6P3tnyAd2KarHGESZz <---

    Jedenfalls.. Trotzdem guten Appetit! 😋 🇭 🇮 🇱 🇫 🇪
    HTML
    %}
    {{ messages.split("\n---\n") }}
    {% end %}
  end

  property webhook : String = ""
  property date : Time = Time.local
  property location : String = "27"

  # Parse options from CLI
  def parse_options(args = ARGV)
    OptionParser.parse do |parser|
      parser.banner = "Usage: kstw_canteen_bot [arguments]"
      parser.on("--webhook PATH", "Sets the webhook path") { |string| @webhook = string }
      parser.on("--date DATE", "Sets the requested date [format: YYYY-MM-DD] (default: NOW)") { |string| @date = Time.parse_utc(string, "%F") }
      parser.on("--location NAME", "Sets the requested location [format: ID or NAME] (default: 22)") { |string| @location = string }
      parser.on("-h", "--help", "Show this help") do
        puts parser
        exit
      end
      parser.invalid_option do |flag|
        STDERR.puts "ERROR: #{flag} is not a valid option."
        STDERR.puts parser
        exit(1)
      end
    end
  end

  # Send webhook using options given
  def main : Nil
    # Fetch dishes
    dishes = Scraper.fetch_dishes(date: @date, location: @location)
    return if dishes.empty?

    # Generate response
    io = IO::Memory.new
    JSON.build(io: io) do |json|
      json.object do
        json.field "content", MESSAGES[rand(MESSAGES.size)]
        json.field "username", USERNAME
        json.field "avatar_url", AVATAR
        json.field "embeds" do
          json.array do
            dishes.each do |dish|
              menu_line = MENU_LINES[dish.menu_line]? || MENU_LINES["FALLBACK"]

              json.object do
                json.field "title", dish.name
                unless dish.description.empty?
                  json.field "description", dish.description
                end
                json.field "color", menu_line["color"]
                json.field "fields" do
                  json.array do
                    if dish.prices
                      json.object do
                        json.field "name", PRICE_STRING
                        json.field "value", dish.prices
                      end
                    end
                    if dish.location_part
                      json.object do
                        json.field "name", LOCATION_STRING
                        json.field "value", dish.location_part
                      end
                    end
                    if (tb = dish.time_begin) && (te = dish.time_end)
                      json.object do
                        json.field "name", TIME_STRING
                        json.field "value", TIME_TEMPLATE.call(tb, te)
                      end
                    end
                  end
                end
                json.field "footer" do
                  json.object do
                    json.field "text", ADDITIVES_TEMPLATE.call(dish.additives)
                    json.field "icon_url", menu_line["icon"]
                  end
                end
                unless dish.image_url.empty?
                  json.field "thumbnail" do
                    json.object do
                      json.field "url", dish.image_url
                    end
                  end
                end
              end
            end
          end
        end
      end
    end

    headers = HTTP::Headers.new
    headers["Content-Type"] = "application/json"
    io.rewind
    puts io
    io.rewind

    puts "\n\nRESPONSE"

    # Send webhook
    res = HTTP::Client.post(@webhook, body: io, headers: headers)
    puts res.body
  end
end

app = App.new
app.parse_options
app.main
